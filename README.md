# Bestimmung der Lichtgeschwindigkeit

[Projektlabor Wiki](http://heide.pl-physik.tu-berlin.de/plwiki/index.php/Lichtgeschwindigkeit)

## Messungen
| Nr. | Frequenz [Hz] | Verschiebung [mm] | Lichtgeschwindigkeit [10^8 m/s] |
|-----|---------------|-------------------|---------------------------------|
| 1   | 177&pm;2      | 1.0&pm;0.2        | 3.3&pm;0.7                      |
| 2   | 161&pm;10     | 0.83&pm;0.02      | 3.7&pm;0.3                      |
| 3   | 192&pm;1      | 1.16&pm;0.02      | 3.12&pm;0.14                    |
| 4   | 136&pm;1      | 0.87&pm;0.02      | 2.94&pm;0.14                    |
| 5   | 84&pm;1       | 0.54&pm;0.02      | 2.92&pm;0.17                    |
| 6   | 256&pm;1      | 1.59&pm;0.02      | 3.04&pm;0.13                    |

=> c = (3.05 &pm; 0.07) &#183; 10^8 m/s

## Verwendung des Programmes
 - Python installieren
 - `pip install numpy`
 - `pip install opencv-python`
 - Programm und Bilder in den gleichen Ordner legen, Programm starten und Eingaben tätigen
 - **Achtung: Bilder müssen gleiche Größe haben**
