import cv2
import numpy as np
import os

### CONSTANTS ###
# image processing constants
MAX_STATIC_Y = 2000
MIN_DYNAMIC_Y = 3000

PXL_PER_MM = 4608 / 13.2

# calculation constants
FOCUSLENGTH = 5
FREQUENCY = 256

# default paths
IMAGE_REF_STATIC = 'referenz_statisch.JPG'
IMAGE_REF_DYNAMIC = 'referenz_dynamisch.JPG'
IMAGE_REF = 'referenz.JPG'
IMAGE_IMG = 'messung.JPG'
IMAGE_OUT = 'result.png'


### USER INPUT ###
print('To use defaults, press enter.')
#inp_rs = input('Reference for the static marker: ')
#if inp_rs:
#    IMAGE_REF_STATIC = inp_rs
#inp_rd = input('Reference for the dynamic marker: ')
#if inp_rd:
#    IMAGE_REF_DYNAMIC = inp_rd
#inp_r = input('Reference image for the relative shift: ')
#if inp_r:
#    IMAGE_REF = inp_r
inp_o = input('Save output(s) as/in: ')
if inp_o:
    IMAGE_OUT = inp_o
inp_i = input('Path of the measurement image(s): ')
if inp_i:
    IMAGE_IMG = inp_i
inp_f = input('Frequency: ')
if inp_f:
    FREQUENCY = float(inp_f)
inp_show = input('show images? (y/n)') == 'y'
inp_save = input('save results? (y/n)') == 'y'

img_files = []
if os.path.isfile(IMAGE_IMG):
    img_files = [IMAGE_IMG]
elif os.path.isdir(IMAGE_IMG):
    img_files = os.listdir(IMAGE_IMG)
    img_files = [f'{IMAGE_IMG}/{f}' for f in img_files if f[-4:] in ['.png', '.PNG', '.jpg', '.JPG']]


ref_static = cv2.imread(IMAGE_REF_STATIC)
ref_dynamic = cv2.imread(IMAGE_REF_DYNAMIC)

distances = []
for img_path in img_files:
    ref = cv2.imread(IMAGE_REF)
    img = cv2.imread(img_path)


    ### STATIC MARKER ###
    img_static_area = img[0:MAX_STATIC_Y, :]
    ref_static_area = ref[0:MAX_STATIC_Y, :]

    # search for the static marker in both images
    img_result = cv2.matchTemplate(img_static_area, ref_static, cv2.TM_CCOEFF_NORMED)
    ref_result = cv2.matchTemplate(ref_static_area, ref_static, cv2.TM_CCOEFF_NORMED)

    _, img_best, _, img_bestLoc_static = cv2.minMaxLoc(img_result)
    _, ref_best, _, ref_bestLoc_static = cv2.minMaxLoc(ref_result)

    # get the difference between static markers
    diff = np.array(img_bestLoc_static) - np.array(ref_bestLoc_static)


    ### DISPLAY-IMAGE ###
    if inp_show or inp_save:
        show_img = ref.copy()
        show_img[:,:,0] = show_img[:,:,2] // 2
        show_img[:,:,1] = 0

        # overlay the second image
        img_rows,img_cols = img.shape[:2]
        y_min = max(0,diff[1])
        y_max =  min(img_rows + diff[1], img_rows)
        x_min = max(0,diff[0])
        x_max =  min(img_cols + diff[0], img_cols)

        moved_img = img.copy()[y_min:y_max,x_min:x_max]
        moved_img[:,:,0] = moved_img[:,:,2]//2
        moved_img[:,:,1] = moved_img[:,:,2]
        moved_img[:,:,2] = 0

        show_img[y_min-diff[1]:y_max-diff[1],x_min-diff[0]:x_max-diff[0]] += moved_img


    ### DYNAMIC MARKERS ###
    img_dynamic_area = img[MIN_DYNAMIC_Y:, :]
    ref_dynamic_area = ref[MIN_DYNAMIC_Y:, :]

    # search for the dynamic marker in both images
    img_result = cv2.matchTemplate(img_dynamic_area, ref_dynamic, cv2.TM_CCOEFF_NORMED)
    ref_result = cv2.matchTemplate(ref_dynamic_area, ref_dynamic, cv2.TM_CCOEFF_NORMED)

    _, img_best, _, img_bestLoc_dynamic = cv2.minMaxLoc(img_result)
    _, ref_best, _, ref_bestLoc_dynamic = cv2.minMaxLoc(ref_result)

    # get the adjusted differnce between dynamic markers
    diff_dynamic = np.array(img_bestLoc_dynamic) - np.array(ref_bestLoc_dynamic) - diff


    ### DRAW BOXES ###
    trows,tcols = ref_dynamic.shape[:2]
    prd_x,prd_y = ref_bestLoc_dynamic
    prd_y += MIN_DYNAMIC_Y
    pid_x,pid_y = img_bestLoc_dynamic
    pid_x += -diff[0]
    pid_y += MIN_DYNAMIC_Y - diff[1]
    if inp_show or inp_save:
        cv2.rectangle(show_img, (prd_x,prd_y),(prd_x+tcols,prd_y+trows),(0,0,255),5)
        cv2.rectangle(show_img, (pid_x,pid_y),(pid_x+tcols,pid_y+trows),(0,0,255),5)

        cv2.line(show_img, (prd_x+tcols//2, prd_y+trows//2), (pid_x+tcols//2, pid_y+trows//2), (255,0,0), 10)
        cv2.circle(show_img, (prd_x+tcols//2, prd_y+trows//2), 10, (255,0,0), thickness=10)
        cv2.circle(show_img, (pid_x+tcols//2, pid_y+trows//2), 10, (255,0,0), thickness=10)

        trows,tcols = ref_static.shape[:2]
        prs_x,prs_y = ref_bestLoc_static
        cv2.rectangle(show_img, (prs_x,prs_y),(prs_x+tcols,prs_y+trows),(0,0,255),5)


    ### CALCULATE C ###
    distance_px = np.sqrt(np.power(prd_x-pid_x,2) + np.power(prd_y-pid_y,2))
    #distance_px = np.abs(prd_x-pid_x)
    distance_mm = distance_px / PXL_PER_MM
    distance = distance_mm / 1000

    c = 24 * np.pi * FOCUSLENGTH*FOCUSLENGTH * FREQUENCY / distance
    print(img_path)
    print(f'{distance_mm=}')
    print(f'{c=}\n')
    distances.append(distance_mm)


    ### SHOW & SAVE IMAGE ###
    if inp_show or inp_save:
        trows,tcols = show_img.shape[:2]
        show_img = cv2.resize(show_img, (tcols//5, trows//5))

        cv2.putText(show_img, f'Frequenz: {FREQUENCY:,.2f}Hz', (40,400), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), thickness=1)
        cv2.putText(show_img, f'Delta_x = {distance_px:.2f}PX = {distance_mm:.2f}mm', (40,450), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), thickness=1)
        cv2.putText(show_img, f'c = {c:,.2f}m/s', (40,500), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), thickness=1)

    if inp_show:
        cv2.imshow('output', show_img)
        cv2.waitKey(0)

    if inp_save:
        cv2.imwrite(f'{IMAGE_OUT}/{img_path.split("/")[-1]}', show_img)

### PRINT RESULTS & ERROR ###
num_meas = len(distances)
if num_meas > 1:
    distances = np.array(distances)
    print(f'{distances=}')
    avg_distance = np.sum(distances)/num_meas
    print(f'{avg_distance=}mm')
    standard_deviation = np.sqrt(np.sum(np.power(avg_distance - distances, 2)) / (num_meas-1))
    print(f'{standard_deviation=}mm')
    avg_error = standard_deviation / np.sqrt(num_meas)
    print(f'{avg_error=}')
    relative_error = avg_error/avg_distance
    print(f'{relative_error=}')
